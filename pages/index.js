import React from 'react'
import LayoutMain from '../components/LayoutMain'
import Head from 'next/head'

const Homepage = () => {

  const homePage = {
    website: "javascript:void(0)",
  };

  return (
    <LayoutMain title='Home'>
      <div>
        <div id="preloader">
          <div className="sk-spinner sk-spinner-wave">
            <div className="sk-rect1" />
            <div className="sk-rect2" />
            <div className="sk-rect3" />
            <div className="sk-rect4" />
            <div className="sk-rect5" />
          </div>
        </div>
        {/* End Preload */}
        <div className="layer" />
        {/* Mobile menu overlay mask */}
        {/* Header================================================== */}
        <header>
          <div id="top_line">
            <div className="container">
              <div className="row">
                <div className="col-6">
                  <i className="icon-phone" />
                  <strong>0045 043204434</strong>
                </div>
                <div className="col-6">
                  <ul id="top_links">
                    <li>
                      <a href="#sign-in-dialog" id="access_link">
                        Sign in
                      </a>
                    </li>
                    <li>
                      <a href="wishlist.html" id="wishlist_link">
                        Wishlist
                      </a>
                    </li>
                    <li>
                      <a href="https://1.envato.market/ryzjQ" target="_parent">
                        Purchase this template
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              {/* End row */}
            </div>
            {/* End container*/}
          </div>
          {/* End top line*/}
          <div className="container">
            <div className="row">
              <div className="col-3">
                <div id="logo_home">
                  <h1>
                    <a href="index.html" title="City tours travel template">
                      City Tours travel template
                    </a>
                  </h1>
                </div>
              </div>
              <nav className="col-9">
                <a className="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);">
                  <span>Menu mobile</span>
                </a>
                <div className="main-menu">
                  <div id="header_menu">
                    <img src="/static/assets/img/logo_sticky.png" width={160} height={34} alt="City tours" data-retina="true" />
                  </div>
                  <a href="#" className="open_close" id="close_in">
                    <i className="icon_set_1_icon-77" />
                  </a>
                  <ul>
                    <li className="submenu">
                      <a href="javascript:void(0);" className="show-submenu">
                        Home <i className="icon-down-open-mini" />
                      </a>
                      <ul>
                        <li>
                          <a href="index.html">Owl Carousel Slider</a>
                        </li>
                        <li>
                          <a href="index_22.html">Home items with Carousel</a>
                        </li>
                        <li>
                          <a href="index_23.html">Home with Search V2</a>
                        </li>
                        <li className="third-level">
                          <a href="javascript:void(0);">Revolution slider</a>
                          <ul>
                            <li>
                              <a href="index_24.html">Default slider</a>
                            </li>
                            <li>
                              <a href="index_20.html">Basic slider</a>
                            </li>
                            <li>
                              <a href="index_14.html">Youtube Hero</a>
                            </li>
                            <li>
                              <a href="index_15.html">Vimeo Hero</a>
                            </li>
                            <li>
                              <a href="index_17.html">Youtube 4K</a>
                            </li>
                            <li>
                              <a href="index_16.html">Carousel</a>
                            </li>
                            <li>
                              <a href="index_19.html">Mailchimp Newsletter</a>
                            </li>
                            <li>
                              <a href="index_18.html">Fixed Caption</a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <a href="index_12.html">Layer slider</a>
                        </li>
                        <li>
                          <a href="index_2.html">With Only tours</a>
                        </li>
                        <li>
                          <a href="index_3.html">Single image</a>
                        </li>
                        <li>
                          <a href="index_4.html">Header video</a>
                        </li>
                        <li>
                          <a href="index_7.html">With search panel</a>
                        </li>
                        <li>
                          <a href="index_13.html">With tabs</a>
                        </li>
                        <li>
                          <a href="index_5.html">With map</a>
                        </li>
                        <li>
                          <a href="index_6.html">With search bar</a>
                        </li>
                        <li>
                          <a href="index_8.html">Search bar + Video</a>
                        </li>
                        <li>
                          <a href="index_9.html">With Text Rotator</a>
                        </li>
                        <li>
                          <a href="index_10.html">With Cookie Bar (EU law)</a>
                        </li>
                        <li>
                          <a href="index_11.html">Popup Advertising</a>
                        </li>
                      </ul>
                    </li>
                    <li className="submenu">
                      <a href="javascript:void(0);" className="show-submenu">
                        Tours <i className="icon-down-open-mini" />
                      </a>
                      <ul>
                        <li>
                          <a href="all_tours_list.html">All tours list</a>
                        </li>
                        <li>
                          <a href="all_tours_grid.html">All tours grid</a>
                        </li>
                        <li>
                          <a href="all_tours_grid_masonry.html">
                            All tours Sort Masonry
                          </a>
                        </li>
                        <li>
                          <a href="all_tours_map_listing.html">
                            All tours map listing
                          </a>
                        </li>
                        <li>
                          <a href="single_tour.html">Single tour page</a>
                        </li>
                        <li>
                          <a href="single_tour_with_gallery.html">
                            Single tour with gallery
                          </a>
                        </li>
                        <li className="third-level">
                          <a href="javascript:void(0);">Single tour fixed sidebar</a>
                          <ul>
                            <li>
                              <a href="single_tour_fixed_sidebar.html">
                                Single tour fixed sidebar
                              </a>
                            </li>
                            <li>
                              <a href="single_tour_with_gallery_fixed_sidebar.html">
                                Single tour 2 Fixed Sidebar
                              </a>
                            </li>
                            <li>
                              <a href="cart_fixed_sidebar.html">Cart Fixed Sidebar</a>
                            </li>
                            <li>
                              <a href="payment_fixed_sidebar.html">
                                Payment Fixed Sidebar
                              </a>
                            </li>
                            <li>
                              <a href="confirmation_fixed_sidebar.html">
                                Confirmation Fixed Sidebar
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <a href="single_tour_working_booking.php">
                            Single tour working booking
                          </a>
                        </li>
                        <li>
                          <a href="single_tour_datepicker_v2.html">
                            Date and time picker V2
                          </a>
                        </li>
                        <li>
                          <a href="cart.html">Single tour cart</a>
                        </li>
                        <li>
                          <a href="payment.html">Single tour booking</a>
                        </li>
                      </ul>
                    </li>
                    <li className="submenu">
                      <a href="javascript:void(0);" className="show-submenu">
                        Hotels <i className="icon-down-open-mini" />
                      </a>
                      <ul>
                        <li>
                          <a href="all_hotels_list.html">All hotels list</a>
                        </li>
                        <li>
                          <a href="all_hotels_grid.html">All hotels grid</a>
                        </li>
                        <li>
                          <a href="all_hotels_grid_masonry.html">
                            All hotels Sort Masonry
                          </a>
                        </li>
                        <li>
                          <a href="all_hotels_map_listing.html">
                            All hotels map listing
                          </a>
                        </li>
                        <li>
                          <a href="single_hotel.html">Single hotel page</a>
                        </li>
                        <li>
                          <a href="single_hotel_datepicker_adv.html">
                            Single hotel datepicker adv
                          </a>
                        </li>
                        <li>
                          <a href="single_hotel_datepicker_v2.html">
                            Date and time picker V2
                          </a>
                        </li>
                        <li>
                          <a href="single_hotel_working_booking.php">
                            Single hotel working booking
                          </a>
                        </li>
                        <li>
                          <a href="single_hotel_contact.php">
                            Single hotel contact working
                          </a>
                        </li>
                        <li>
                          <a href="cart_hotel.html">Cart hotel</a>
                        </li>
                        <li>
                          <a href="payment_hotel.html">Booking hotel</a>
                        </li>
                        <li>
                          <a href="confirmation_hotel.html">Confirmation hotel</a>
                        </li>
                      </ul>
                    </li>
                    <li className="submenu">
                      <a href="javascript:void(0);" className="show-submenu">
                        Transfers <i className="icon-down-open-mini" />
                      </a>
                      <ul>
                        <li>
                          <a href="all_transfer_list.html">All transfers list</a>
                        </li>
                        <li>
                          <a href="all_transfer_grid.html">All transfers grid</a>
                        </li>
                        <li>
                          <a href="all_transfer_grid_masonry.html">
                            All transfers Sort Masonry
                          </a>
                        </li>
                        <li>
                          <a href="single_transfer.html">Single transfer page</a>
                        </li>
                        <li>
                          <a href="single_transfer_datepicker_v2.html">
                            Date and time picker V2
                          </a>
                        </li>
                        <li>
                          <a href="cart_transfer.html">Cart transfers</a>
                        </li>
                        <li>
                          <a href="payment_transfer.html">Booking transfers</a>
                        </li>
                        <li>
                          <a href="confirmation_transfer.html">
                            Confirmation transfers
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="submenu">
                      <a href="javascript:void(0);" className="show-submenu">
                        Restaurants <i className="icon-down-open-mini" />
                      </a>
                      <ul>
                        <li>
                          <a href="all_restaurants_list.html">All restaurants list</a>
                        </li>
                        <li>
                          <a href="all_restaurants_grid.html">All restaurants grid</a>
                        </li>
                        <li>
                          <a href="all_restaurants_grid_masonry.html">
                            All restaurants Sort Masonry
                          </a>
                        </li>
                        <li>
                          <a href="all_restaurants_map_listing.html">
                            All restaurants map listing
                          </a>
                        </li>
                        <li>
                          <a href="single_restaurant.html">Single restaurant page</a>
                        </li>
                        <li>
                          <a href="single_restaurant_datepicker_v2.html">
                            Date and time picker V2
                          </a>
                        </li>
                        <li>
                          <a href="payment_restaurant.html">Booking restaurant</a>
                        </li>
                        <li>
                          <a href="confirmation_restaurant.html">
                            Confirmation transfers
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="megamenu submenu">
                      <a href="javascript:void(0);" className="show-submenu-mega">
                        Bonus
                        <i className="icon-down-open-mini" />
                      </a>
                      <div className="menu-wrapper">
                        <div className="row">
                          <div className="col-lg-4">
                            <h3>Header styles</h3>
                            <ul>
                              <li>
                                <a href="index.html">Default transparent</a>
                              </li>
                              <li>
                                <a href="header_2.html">Plain color</a>
                              </li>
                              <li>
                                <a href="header_3.html">Plain color on scroll</a>
                              </li>
                              <li>
                                <a href="header_4.html">With socials on top</a>
                              </li>
                              <li>
                                <a href="header_5.html">With language selection</a>
                              </li>
                              <li>
                                <a href="header_6.html">With lang and conversion</a>
                              </li>
                              <li>
                                <a href="header_7.html">With full horizontal menu</a>
                              </li>
                            </ul>
                          </div>
                          <div className="col-lg-4">
                            <h3>Footer styles</h3>
                            <ul>
                              <li>
                                <a href="index.html">Footer default</a>
                              </li>
                              <li>
                                <a href="footer_2.html">Footer style 2</a>
                              </li>
                              <li>
                                <a href="footer_3.html">Footer style 3</a>
                              </li>
                              <li>
                                <a href="footer_4.html">Footer style 4</a>
                              </li>
                              <li>
                                <a href="footer_5.html">Footer style 5</a>
                              </li>
                              <li>
                                <a href="footer_6.html">Footer style 6</a>
                              </li>
                              <li>
                                <a href="footer_7.html">Footer style 7</a>
                              </li>
                            </ul>
                          </div>
                          <div className="col-lg-4">
                            <h3>Shop section</h3>
                            <ul>
                              <li>
                                <a href="shop.html">Shop</a>
                              </li>
                              <li>
                                <a href="shop-single.html">Shop single</a>
                              </li>
                              <li>
                                <a href="shopping-cart.html">Shop cart</a>
                              </li>
                              <li>
                                <a href="checkout.html">Shop Checkout</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        {/* End row */}
                      </div>
                      {/* End menu-wrapper */}
                    </li>
                    <li className="megamenu submenu">
                      <a href="javascript:void(0);" className="show-submenu-mega">
                        Pages
                        <i className="icon-down-open-mini" />
                      </a>
                      <div className="menu-wrapper">
                        <div className="row">
                          <div className="col-lg-4">
                            <h3>Pages</h3>
                            <ul>
                              <li>
                                <a href="about.html">About us</a>
                              </li>
                              <li>
                                <a href="general_page.html">General page</a>
                              </li>
                              <li>
                                <a href="tourist_guide.html">Tourist guide</a>
                              </li>
                              <li>
                                <a href="wishlist.html">Wishlist page</a>
                              </li>
                              <li>
                                <a href="faq.html">Faq</a>
                              </li>
                              <li>
                                <a href="faq_2.html">Faq smooth scroll</a>
                              </li>
                              <li>
                                <a href="pricing_tables.html">Pricing tables</a>
                              </li>
                              <li>
                                <a href="gallery_3_columns.html">Gallery 3 columns</a>
                              </li>
                              <li>
                                <a href="gallery_4_columns.html">Gallery 4 columns</a>
                              </li>
                              <li>
                                <a href="grid_gallery_1.html">Grid gallery</a>
                              </li>
                              <li>
                                <a href="grid_gallery_2.html">
                                  Grid gallery with filters
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div className="col-lg-4">
                            <h3>Pages</h3>
                            <ul>
                              <li>
                                <a href="contact_us_1.html">Contact us 1</a>
                              </li>
                              <li>
                                <a href="contact_us_2.html">Contact us 2</a>
                              </li>
                              <li>
                                <a href="blog_right_sidebar.html">Blog</a>
                              </li>
                              <li>
                                <a href="blog.html">Blog left sidebar</a>
                              </li>
                              <li>
                                <a href="login.html">Login</a>
                              </li>
                              <li>
                                <a href="register.html">Register</a>
                              </li>
                              <li>
                                <a href="invoice.html" target="_blank">
                                  Invoice
                                </a>
                              </li>
                              <li>
                                <a href="404.html">404 Error page</a>
                              </li>
                              <li>
                                <a href="site_launch/index.html">
                                  Site launch / Coming soon
                                </a>
                              </li>
                              <li>
                                <a href="timeline.html">Tour timeline</a>
                              </li>
                              <li>
                                <a href="page_with_map.html">
                                  <i className="icon-map" /> Full screen map
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div className="col-lg-4">
                            <h3>Elements</h3>
                            <ul>
                              <li>
                                <a href="footer_2.html">
                                  <i className="icon-columns" /> Footer with working
                                  newsletter
                                </a>
                              </li>
                              <li>
                                <a href="footer_5.html">
                                  <i className="icon-columns" /> Footer with Twitter
                                  feed
                                </a>
                              </li>
                              <li>
                                <a href="icon_pack_1.html">
                                  <i className="icon-inbox-alt" /> Icon pack 1 (1900)
                                </a>
                              </li>
                              <li>
                                <a href="icon_pack_2.html">
                                  <i className="icon-inbox-alt" /> Icon pack 2 (100)
                                </a>
                              </li>
                              <li>
                                <a href="icon_pack_3.html">
                                  <i className="icon-inbox-alt" /> Icon pack 3 (30)
                                </a>
                              </li>
                              <li>
                                <a href="icon_pack_4.html">
                                  <i className="icon-inbox-alt" /> Icon pack 4 (200)
                                </a>
                              </li>
                              <li>
                                <a href="icon_pack_5.html">
                                  <i className="icon-inbox-alt" /> Icon pack 5 (360)
                                </a>
                              </li>
                              <li>
                                <a href="shortcodes.html">
                                  <i className="icon-tools" /> Shortcodes
                                </a>
                              </li>
                              <li>
                                <a href="newsletter_template/newsletter.html" target="blank">
                                  <i className=" icon-mail" /> Responsive email
                                  template
                                </a>
                              </li>
                              <li>
                                <a href="admin.html">
                                  <i className="icon-cog-1" /> Admin area
                                </a>
                              </li>
                              <li>
                                <a href="html_rtl/index.html">
                                  <i className="icon-align-right" /> RTL Version
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        {/* End row */}
                      </div>
                      {/* End menu-wrapper */}
                    </li>
                  </ul>
                </div>
                {/* End main-menu */}
                <ul id="top_tools">
                  <li>
                    <a href="javascript:void(0);" className="search-overlay-menu-btn">
                      <i className="icon_search" />
                    </a>
                  </li>
                  <li>
                    <div className="dropdown dropdown-cart">
                      <a href="#" data-toggle="dropdown" className="cart_bt">
                        <i className="icon_bag_alt" />
                        <strong>3</strong>
                      </a>
                      <ul className="dropdown-menu" id="cart_items">
                        <li>
                          <div className="image">
                            <img src="/static/assets/img/thumb_cart_1.jpg" alt="image" />
                          </div>
                          <strong>
                            <a href="#">Louvre museum</a>1x $36.00{" "}
                          </strong>
                          <a href="#" className="action">
                            <i className="icon-trash" />
                          </a>
                        </li>
                        <li>
                          <div className="image">
                            <img src="/static/assets/img/thumb_cart_2.jpg" alt="image" />
                          </div>
                          <strong>
                            <a href="#">Versailles tour</a>2x $36.00{" "}
                          </strong>
                          <a href="#" className="action">
                            <i className="icon-trash" />
                          </a>
                        </li>
                        <li>
                          <div className="image">
                            <img src="/static/assets/img/thumb_cart_3.jpg" alt="image" />
                          </div>
                          <strong>
                            <a href="#">Versailles tour</a>1x $36.00{" "}
                          </strong>
                          <a href="#" className="action">
                            <i className="icon-trash" />
                          </a>
                        </li>
                        <li>
                          <div>
                            Total: <span>$120.00</span>
                          </div>
                          <a href="cart.html" className="button_drop">
                            Go to cart
                          </a>
                          <a href="payment.html" className="button_drop outline">
                            Check out
                          </a>
                        </li>
                      </ul>
                    </div>
                    {/* End dropdown-cart*/}
                  </li>
                </ul>
              </nav>
            </div>
          </div>
          {/* container */}
        </header>
        {/* End Header */}
        <main>
          <section className="header-video">
            <div id="hero_video">
              <div className="intro_title">
                <h3 className="animated fadeInDown">Affordable Paris tours</h3>
                <p className="animated fadeInDown">
                  City Tours / Tour Tickets / Tour Guides
                </p>
                <a href="#" className="animated fadeInUp button_intro">
                  View Tours
                </a>
                <a href="#" className="animated fadeInUp button_intro outline hidden-sm hidden-xs">
                  View Tickets
                </a>
                <a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y"
                  className="video animated fadeInUp button_intro outline">
                  Play video
                </a>
              </div>
            </div>
            <img src alt="Image" className="header-video--media" data-video-src data-teaser-source="/static/assets/video/paris"
              data-provider="Youtube" data-video-width={854} data-video-height={480} />
          </section>
          {/* End Header video */}
          <div className="container margin_60">
            <div className="main_title">
              <h2>
                Paris <span>Top</span> Tours
              </h2>
              <p>
                Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur
                consequat.
              </p>
            </div>
            <div className="row">
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                <div className="tour_container">
                  <div className="ribbon_3 popular">
                    <span>Popular</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_1.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-44" />
                        Historic Buildings
                        <span className="price">
                          <sup>$</sup>39
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Arc Triomphe</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.2s">
                <div className="tour_container">
                  <div className="ribbon_3 popular">
                    <span>Popular</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_2.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="badge_save">
                        Save<strong>30%</strong>
                      </div>
                      <div className="short_info">
                        <i className="icon_set_1_icon-43" />
                        Churches
                        <span className="price">
                          <sup>$</sup>45
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Notredame</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                <div className="tour_container">
                  <div className="ribbon_3 popular">
                    <span>Popular</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_3.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-44" />
                        Historic Buildings
                        <span className="price">
                          <sup>$</sup>48
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Versailles</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.4s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_4.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="badge_save">
                        Save<strong>30%</strong>
                      </div>
                      <div className="short_info">
                        <i className="icon_set_1_icon-30" />
                        Walking tour
                        <span className="price">
                          <sup>$</sup>36
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Pompidue</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.5s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_14.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-28" />
                        Skyline tours
                        <span className="price">
                          <sup>$</sup>42
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Tour Eiffel</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_5.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-44" />
                        Historic Buildings
                        <span className="price">
                          <sup>$</sup>40
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Pantheon</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.7s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_8.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-3" />
                        City sightseeing
                        <span className="price">
                          <sup>$</sup>35
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Open Bus</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.8s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_9.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-4" />
                        Museums
                        <span className="price">
                          <sup>$</sup>38
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Louvre museum</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
              <div className="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                <div className="tour_container">
                  <div className="ribbon_3">
                    <span>Top rated</span>
                  </div>
                  <div className="img_container">
                    <a href="single_tour.html">
                      <img src="/static/assets/img/tour_box_12.jpg" width={800} height={533} className="img-fluid" alt="Image" />
                      <div className="short_info">
                        <i className="icon_set_1_icon-14" />
                        Eat &amp; drink
                        <span className="price">
                          <sup>$</sup>25
                        </span>
                      </div>
                    </a>
                  </div>
                  <div className="tour_title">
                    <h3>
                      <strong>Boulangerie</strong> tour
                    </h3>
                    <div className="rating">
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile voted" />
                      <i className="icon-smile" />
                      <small>(75)</small>
                    </div>
                    {/* end rating */}
                    <div className="wishlist">
                      <a className="tooltip_flip tooltip-effect-1" href="javascript:void(0);">
                        +
                        <span className="tooltip-content-flip">
                          <span className="tooltip-back">Add to wishlist</span>
                        </span>
                      </a>
                    </div>
                    {/* End wish list*/}
                  </div>
                </div>
                {/* End box tour */}
              </div>
              {/* End col */}
            </div>
            {/* End row */}
            <p className="text-center nopadding">
              <a href="#" className="btn_1 medium">
                <i className="icon-eye-7" />
                View all tours (144){" "}
              </a>
            </p>
          </div>
          {/* End container */}
          <div className="white_bg">
            <div className="container margin_60">
              <div className="main_title">
                <h2>
                  Other <span>Popular</span> tours
                </h2>
                <p>
                  Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur
                  consequat.
                </p>
              </div>
              <div className="row add_bottom_45">
                <div className="col-lg-4 other_tours">
                  <ul>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-3" />
                        Tour Eiffel<span className="other_tours_price">$42</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-30" />
                        Shopping tour<span className="other_tours_price">$35</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-44" />
                        Versailles tour<span className="other_tours_price">$20</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-3" />
                        Montparnasse skyline
                        <span className="other_tours_price">$26</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-44" />
                        Pompidue<span className="other_tours_price">$26</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-3" />
                        Senna River tour<span className="other_tours_price">$32</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-4 other_tours">
                  <ul>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-1" />
                        Notredame<span className="other_tours_price">$48</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-4" />
                        Lafaiette<span className="other_tours_price">$55</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-30" />
                        Trocadero<span className="other_tours_price">$76</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-3" />
                        Open Bus tour<span className="other_tours_price">$55</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-30" />
                        Louvre museum<span className="other_tours_price">$24</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-3" />
                        Madlene Cathedral
                        <span className="other_tours_price">$24</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-4 other_tours">
                  <ul>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-37" />
                        Montparnasse<span className="other_tours_price">$36</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-1" />
                        D'Orsey museum<span className="other_tours_price">$28</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-50" />
                        Gioconda Louvre musuem
                        <span className="other_tours_price">$44</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-44" />
                        Tour Eiffel<span className="other_tours_price">$56</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-50" />
                        Ladefanse<span className="other_tours_price">$16</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon_set_1_icon-44" />
                        Notredame<span className="other_tours_price">$26</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              {/* End row */}
              <div className="banner colored">
                <h4>
                  Discover our Top tours <span>from $34</span>
                </h4>
                <p>
                  Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                  efficiantur in.
                </p>
                <a href="single_tour.html" className="btn_1 white">
                  Read more
                </a>
              </div>
              <div className="row">
                <div className="col-lg-3 col-md-6 text-center">
                  <p>
                    <a href="#">
                      <img src="/static/assets/img/bus.jpg" alt="Pic" className="img-fluid" />
                    </a>
                  </p>
                  <h4>
                    <span>Sightseen tour</span> booking
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset, doctus
                    volumus explicari qui ex.
                  </p>
                </div>
                <div className="col-lg-3 col-md-6 text-center">
                  <p>
                    <a href="#">
                      <img src="/static/assets/img/transfer.jpg" alt="Pic" className="img-fluid" />
                    </a>
                  </p>
                  <h4>
                    <span>Transfer</span> booking
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset, doctus
                    volumus explicari qui ex.
                  </p>
                </div>
                <div className="col-lg-3 col-md-6 text-center">
                  <p>
                    <a href="#">
                      <img src="/static/assets/img/guide.jpg" alt="Pic" className="img-fluid" />
                    </a>
                  </p>
                  <h4>
                    <span>Tour guide</span> booking
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset, doctus
                    volumus explicari qui ex.
                  </p>
                </div>
                <div className="col-lg-3 col-md-6 text-center">
                  <p>
                    <a href="#">
                      <img src="/static/assets/img/hotel.jpg" alt="Pic" className="img-fluid" />
                    </a>
                  </p>
                  <h4>
                    <span>Hotel</span> booking
                  </h4>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset, doctus
                    volumus explicari qui ex.
                  </p>
                </div>
              </div>
              {/* End row */}
            </div>
            {/* End container */}
          </div>
          {/* End white_bg */}
          <section className="promo_full">
            <div className="promo_full_wp magnific">
              <div>
                <h3>BELONG ANYWHERE</h3>
                <p>
                  Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                  efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus
                  explicari qui ex.
                </p>
                <a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" className="video">
                  <i className="icon-play-circled2-1" />
                </a>
              </div>
            </div>
          </section>
          {/* End section */}
          <div className="container margin_60">
            <div className="main_title">
              <h2>
                Some <span>good</span> reasons
              </h2>
              <p>
                Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur
                consequat.
              </p>
            </div>
            <div className="row">
              <div className="col-lg-4 wow zoomIn" data-wow-delay="0.2s">
                <div className="feature_home">
                  <i className="icon_set_1_icon-41" />
                  <h3>
                    <span>+120</span> Premium tours
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset.
                  </p>
                  <a href="about.html" className="btn_1 outline">
                    Read more
                  </a>
                </div>
              </div>
              <div className="col-lg-4 wow zoomIn" data-wow-delay="0.4s">
                <div className="feature_home">
                  <i className="icon_set_1_icon-30" />
                  <h3>
                    <span>+1000</span> Customers
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset.
                  </p>
                  <a href="about.html" className="btn_1 outline">
                    Read more
                  </a>
                </div>
              </div>
              <div className="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
                <div className="feature_home">
                  <i className="icon_set_1_icon-57" />
                  <h3>
                    <span>H24 </span> Support
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                    efficiantur in. Nec id tempor imperdiet deterruisset.
                  </p>
                  <a href="about.html" className="btn_1 outline">
                    Read more
                  </a>
                </div>
              </div>
            </div>
            {/*End row */}
            <hr />
            <div className="row">
              <div className="col-md-6">
                <img src="/static/assets/img/laptop.png" alt="Laptop" className="img-fluid laptop" />
              </div>
              <div className="col-md-6">
                <h3>
                  <span>Get started</span> with CityTours
                </h3>
                <p>
                  Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus
                  efficiantur in. Nec id tempor imperdiet deterruisset.
                </p>
                <ul className="list_order">
                  <li>
                    <span>1</span>Select your preferred tours
                  </li>
                  <li>
                    <span>2</span>Purchase tickets and options
                  </li>
                  <li>
                    <span>3</span>Pick them directly from your office
                  </li>
                </ul>
                <a href="all_tour_list.html" className="btn_1">
                  Start now
                </a>
              </div>
            </div>
            {/* End row */}
          </div>
          {/* End container */}
        </main>
        {/* End main */}
        <footer className="revealed">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <h3>Need help?</h3>
                <a href="tel://004542344599" id="phone">
                  +45 423 445 99
                </a>
                <a href="mailto:help@citytours.com" id="email_footer">
                  help@citytours.com
                </a>
              </div>
              <div className="col-md-3">
                <h3>About</h3>
                <ul>
                  <li>
                    <a href="#">About us</a>
                  </li>
                  <li>
                    <a href="#">FAQ</a>
                  </li>
                  <li>
                    <a href="#">Login</a>
                  </li>
                  <li>
                    <a href="#">Register</a>
                  </li>
                  <li>
                    <a href="#">Terms and condition</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-3">
                <h3>Discover</h3>
                <ul>
                  <li>
                    <a href="#">Community blog</a>
                  </li>
                  <li>
                    <a href="#">Tour guide</a>
                  </li>
                  <li>
                    <a href="#">Wishlist</a>
                  </li>
                  <li>
                    <a href="#">Gallery</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2">
                <h3>Settings</h3>
                <div className="styled-select">
                  <select name="lang" id="lang">
                    <option defaultValue="English" selected>
                      English
                    </option>
                    <option defaultValue="French">French</option>
                    <option defaultValue="Spanish">Spanish</option>
                    <option defaultValue="Russian">Russian</option>
                  </select>
                </div>
                <div className="styled-select">
                  <select name="currency" id="currency">
                    <option defaultValue="USD" selected>
                      USD
                    </option>
                    <option defaultValue="EUR">EUR</option>
                    <option defaultValue="GBP">GBP</option>
                    <option defaultValue="RUB">RUB</option>
                  </select>
                </div>
              </div>
            </div>
            {/* End row */}
            <div className="row">
              <div className="col-md-12">
                <div id="social_footer">
                  <ul>
                    <li>
                      <a href="#">
                        <i className="icon-facebook" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-google" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-instagram" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-pinterest" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-vimeo" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="icon-youtube-play" />
                      </a>
                    </li>
                  </ul>
                  <p>© Citytours 2018</p>
                </div>
              </div>
            </div>
            {/* End row */}
          </div>
          {/* End container */}
        </footer>
        {/* End footer */}
        <div id="toTop" />
        {/* Back to top button */}
        {/* Search Menu */}
        <div className="search-overlay-menu">
          <span className="search-overlay-close">
            <i className="icon_set_1_icon-77" />
          </span>
          <form role="search" id="searchform" method="get">
            <input defaultValue name="q" type="search" placeholder="Search..." />
            <button type="submit">
              <i className="icon_set_1_icon-78" />
            </button>
          </form>
        </div>
        {/* End Search Menu */}
        {/* Sign In Popup */}
        <div id="sign-in-dialog" className="zoom-anim-dialog mfp-hide">
          <div className="small-dialog-header">
            <h3>Sign In</h3>
          </div>
          <form>
            <div className="sign-in-wrapper">
              <a href="#0" className="social_bt facebook">
                Login with Facebook
              </a>
              <a href="#0" className="social_bt google">
                Login with Google
              </a>
              <div className="divider">
                <span>Or</span>
              </div>
              <div className="form-group">
                <label>Email</label>
                <input type="email" className="form-control" name="email" id="email" />
                <i className="icon_mail_alt" />
              </div>
              <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" name="password" id="password" defaultValue />
                <i className="icon_lock_alt" />
              </div>
              <div className="clearfix add_bottom_15">
                <div className="checkboxes float-left">
                  <input id="remember-me" type="checkbox" name="check" />
                  <label htmlFor="remember-me">Remember Me</label>
                </div>
                <div className="float-right">
                  <a id="forgot" href="javascript:void(0);">
                    Forgot Password?
                  </a>
                </div>
              </div>
              <div className="text-center">
                <input type="submit" defaultValue="Log In" className="btn_login" />
              </div>
              <div className="text-center">
                Don’t have an account? <a href="javascript:void(0);">Sign up</a>
              </div>
              <div id="forgot_pw">
                <div className="form-group">
                  <label>Please confirm login email below</label>
                  <input type="email" className="form-control" name="email_forgot" id="email_forgot" />
                  <i className="icon_mail_alt" />
                </div>
                <p>
                  You will receive an email containing a link allowing you to reset
                  your password to a new preferred one.
                </p>
                <div className="text-center">
                  <input type="submit" defaultValue="Reset Password" className="btn_1" />
                </div>
              </div>
            </div>
          </form>
          {/*form */}
        </div>
        {/* /Sign In Popup */}
      </div>
    </LayoutMain>
  )
}

export default Homepage;